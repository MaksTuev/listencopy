package ru.litres.android.audio.module.player.event;

import ru.litres.android.audio.entity.Book;
import ru.litres.android.audio.entity.bookstate.PlayerBookState;

/**
 * событие изменения статуса плеера
 */
public class PlayerStatusChangedEvent {

    private Book book;

    private PlayerBookState playerBookState;

    public Book getBook() {
        return book;
    }

    public PlayerBookState getPlayerBookState() {
        return playerBookState;
    }
}
