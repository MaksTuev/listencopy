package ru.litres.android.audio.module.player;

import javax.inject.Inject;

import ru.litres.android.audio.entity.bookstate.PlayerBookState;
import ru.litres.android.audio.entity.bookstate.PlayingStatus;
import ru.litres.android.audio.module.player.storage.StaticPlayerBookStateRepository;
import rx.Observable;

/**
 * предоставляет актуальный {@link PlayerBookState } для соответствующей книги
 */
public class PlayerBookStateProvider {

    private BookAudioPlayer bookAudioPlayer;
    private StaticPlayerBookStateRepository staticPlayerBookStateRepository;

    private PlayerBookState lastEmittedPlayerBookState = null;

    @Inject
    public PlayerBookStateProvider(BookAudioPlayer bookAudioPlayer, StaticPlayerBookStateRepository staticPlayerBookStateRepository) {
        this.bookAudioPlayer = bookAudioPlayer;
        this.staticPlayerBookStateRepository = staticPlayerBookStateRepository;

        observeAudioEvents();
    }

    /**
     * предоставляет актуальный {@link PlayerBookState } для соответствующей книги
     *
     * @param bookId
     */
    public Observable<PlayerBookState> getPlayerBookState(String bookId) {
        if (lastEmittedPlayerBookState != null
                && lastEmittedPlayerBookState.getBookId().equals(bookId)) {
            //если для указанной книги было получено сообщение об изменении ее состояния в плеере,
            //то возвращаем это состояние
            return Observable.just(lastEmittedPlayerBookState);
        } else {
            //иначе возвращаем состояние из локального хранилища
            return staticPlayerBookStateRepository.getStaticPlayerBookState(bookId)
                    .map(staticPlayerBookState -> staticPlayerBookState.transform(PlayingStatus.STOP));
        }
    }


    private void observeAudioEvents() {
        bookAudioPlayer.observeAudioProgress()
                .subscribe(playerBookState -> lastEmittedPlayerBookState = playerBookState);

        bookAudioPlayer.observePlayerStatusChanged()
                .subscribe(event -> lastEmittedPlayerBookState = event.getPlayerBookState());
    }


}
