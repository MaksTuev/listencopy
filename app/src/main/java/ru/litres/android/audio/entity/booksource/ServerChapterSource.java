package ru.litres.android.audio.entity.booksource;

import ru.litres.android.audio.module.repository.BookRepository;

/**
 * класс, содержащий информацию о файле главы книги на сервере
 */
public class ServerChapterSource implements ChapterSource {
    public static final String GROUP_STANDARD_128_KBPS = "3";
    public static final String GROUP_STANDARD_192_KBPS = "5";
    public static final String GROUP_STANDARD_64_KBPS = "7";
    public static final String GROUP_MOBILE_32_KBPS = "6";
    public static final String GROUP_MOBILE_16_KBPS = "4";
    public static final String GROUP_UNCKNOWN = "";


    private String id;

    private int chapter;

    private String groupId;

    private String bookId;

    private long size;

    private long second;
    /**
     * url завист от сесии и обновляется каждый раз при пролучении книги через {@link BookRepository}
    */
    private String url;

    @Override
    public String getSource() {
        return url;
    }

    public boolean isTrial() {
        return chapter == CHAPTER_TRIAL;
    }

    public ServerChapterSource(String id, int chapter, String groupId, String bookId, long size, long second) {
        this.id = id;
        this.chapter = chapter;
        this.groupId = groupId;
        this.bookId = bookId;
        this.size = size;
        this.second = second;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getSecond() {
        return second;
    }

    public long getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "ServerChapterSource{" +
                "id='" + id + '\'' +
                ", chapter=" + chapter +
                ", groupId='" + groupId + '\'' +
                ", bookId='" + bookId + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
