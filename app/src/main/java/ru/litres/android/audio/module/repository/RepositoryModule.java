package ru.litres.android.audio.module.repository;

import dagger.Module;
import dagger.Provides;
import ru.litres.android.audio.ui.app.PerApplication;

@Module
public class RepositoryModule {

    @Provides
    @PerApplication
    BookRepository provideBookRepository(){
        return new BookRepository();
    }
}
