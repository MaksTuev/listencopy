package ru.litres.android.audio.interactor.bookfull;

import javax.inject.Inject;

import ru.litres.android.audio.module.player.PlayerBookStateProvider;
import ru.litres.android.audio.module.repository.BookRepository;

/**
 * класс, поставляющий {@link BookFull}
 * илпользуется только для UI
 */
public class BookFullInteractor {

    private BookRepository bookRepository;
    private PlayerBookStateProvider playerBookStateProvider;

    @Inject
    public BookFullInteractor(BookRepository bookRepository, PlayerBookStateProvider playerBookStateProvider) {
        this.bookRepository = bookRepository;
        this.playerBookStateProvider = playerBookStateProvider;
    }

    //todo
}
