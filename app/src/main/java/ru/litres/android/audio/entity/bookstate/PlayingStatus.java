package ru.litres.android.audio.entity.bookstate;

public enum PlayingStatus {
    PLAY,
    PAUSE,
    STOP,
    BUFFERING
}
