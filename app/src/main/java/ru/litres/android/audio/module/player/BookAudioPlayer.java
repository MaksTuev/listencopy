package ru.litres.android.audio.module.player;

import javax.inject.Inject;

import ru.litres.android.audio.entity.bookstate.PlayerBookState;
import ru.litres.android.audio.module.player.core.AudioPlayerCore;
import ru.litres.android.audio.module.player.event.PlayerStatusChangedEvent;
import rx.Observable;

/**
 * высокоуровневый плеер, позволяющий воспроизводить книги
 */
public class BookAudioPlayer {

    private AudioPlayerCore audioPlayerCore;

    @Inject
    public BookAudioPlayer(AudioPlayerCore audioPlayerCore) {
        this.audioPlayerCore = audioPlayerCore;
    }

    public Observable<PlayerBookState> observeAudioProgress(){
        return Observable.empty(); //todo
    }

    //todo error
    public Observable<PlayerStatusChangedEvent> observePlayerStatusChanged(){
        return Observable.empty(); //todo
    }
}
