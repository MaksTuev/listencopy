package ru.litres.android.audio.ui.base;

public interface HasName {
    String getName();
}
