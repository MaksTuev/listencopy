package ru.litres.android.audio.module;

import android.content.Context;
import android.content.SharedPreferences;

import ru.litres.android.audio.util.log.SettingsUtil;

/**
 * хранилище для текущей сессии пользователя
 */
public class SessionStorage {
    public static final String PREFERENCES_SESSION = "preferences_session";
    private static final String KEY_SESSION = "KEY_SESSION";
    private final SharedPreferences mSessionSettings;
    private Context mAppContext;

    public SessionStorage(Context appContext) {
        this.mAppContext = appContext;
        mSessionSettings = getSessionSharedPreferences(this.mAppContext);
    }

    public String getSession() {
        return SettingsUtil.getString(mAppContext, mSessionSettings, KEY_SESSION);
    }

    public void setSession(String session) {
        SettingsUtil.putString(mAppContext, mSessionSettings, KEY_SESSION, session);
    }

    public void clearSession() {
        SettingsUtil.putString(mAppContext, mSessionSettings, KEY_SESSION, SettingsUtil.EMPTY_STRING_SETTING);
    }

    public boolean isSessionEmpty() {
        return getSession().equals(SettingsUtil.EMPTY_STRING_SETTING);
    }

    private SharedPreferences getSessionSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_SESSION, Context.MODE_PRIVATE);
    }
}