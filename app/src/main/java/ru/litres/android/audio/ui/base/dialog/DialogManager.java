package ru.litres.android.audio.ui.base.dialog;

/**
 * Сущность, отвечающая за показывание и скрывание диалогов
 */
public interface DialogManager {
    void show(BaseDialog dialog);

}
