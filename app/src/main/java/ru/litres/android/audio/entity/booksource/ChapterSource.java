package ru.litres.android.audio.entity.booksource;

public interface ChapterSource {
    int CHAPTER_TRIAL = -1;

    String getSource();
}
