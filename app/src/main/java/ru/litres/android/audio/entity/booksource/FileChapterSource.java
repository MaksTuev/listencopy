package ru.litres.android.audio.entity.booksource;

import java.io.Serializable;

/**
 * класс, содержащий информацию о файле главы книги, сохраненном на устройстве
 */
public class FileChapterSource implements ChapterSource, Serializable {

    /// id книги.
    private String bookId;

    /// номер главы.
    private int chapter;

    /// Путь к файлу (локальный полный путь до файла).
    private String filePath;

    public FileChapterSource(String bookId, int chapter, String filePath) {
        this.bookId = bookId;
        this.chapter = chapter;
        this.filePath = filePath;
    }

    @Override
    public String getSource() {
        return filePath;
    }

    public String getBookId() {
        return bookId;
    }

    public int getChapter() {
        return chapter;
    }

    @Override
    public String toString() {
        return "FileChapterSource{" +
                "bookId='" + bookId + '\'' +
                ", chapter=" + chapter +
                ", filePath='" + filePath + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileChapterSource)) return false;

        FileChapterSource that = (FileChapterSource) o;

        if (chapter != that.chapter) return false;
        if (bookId != null ? !bookId.equals(that.bookId) : that.bookId != null) return false;
        return !(filePath != null ? !filePath.equals(that.filePath) : that.filePath != null);

    }

    @Override
    public int hashCode() {
        int result = bookId != null ? bookId.hashCode() : 0;
        result = 31 * result + chapter;
        result = 31 * result + (filePath != null ? filePath.hashCode() : 0);
        return result;
    }
}
