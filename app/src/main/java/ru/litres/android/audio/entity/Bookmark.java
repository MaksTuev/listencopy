package ru.litres.android.audio.entity;

/**
 * закладка в книге
 */
public class Bookmark implements Comparable<Bookmark> {

    private String id;

    /**
     * показывает, была ли загружена эта закладка на сервер
     */
    private boolean synchronizedWithServer = false;

    private String title;

    private int listenPosition;

    private int listenChapter;

    private String bookUUID;

    public int getListenPosition() {
        return listenPosition;
    }

    public int getListenChapter() {
        return listenChapter;
    }

    public boolean isSynchronized() {
        return synchronizedWithServer;
    }

    public void setSynchronized(boolean synchronizedWithServer) {
        this.synchronizedWithServer = synchronizedWithServer;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBookUUID() {
        return bookUUID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int compareTo(final Bookmark another) {
        final int order = this.getListenChapter() - another.getListenChapter();
        if(order == 0) {
            return this.getListenPosition() - another.getListenPosition();
        }
        return order;
    }

}
