package ru.litres.android.audio.module.player;

import dagger.Module;
import dagger.Provides;
import ru.litres.android.audio.module.player.core.AudioPlayerCore;
import ru.litres.android.audio.module.player.storage.StaticPlayerBookStateRepository;
import ru.litres.android.audio.ui.app.PerApplication;

@Module
public class PlayerModule {

    @Provides
    @PerApplication
    BookAudioPlayer provideBookAudioPlayer(AudioPlayerCore audioPlayerCore){
        return new BookAudioPlayer(audioPlayerCore);
    }

    @Provides
    @PerApplication
    PlayerBookStateProvider providePlayerBookStateProvider(BookAudioPlayer bookAudioPlayer,
                                                           StaticPlayerBookStateRepository staticPlayerBookStateRepository){
        return new PlayerBookStateProvider(bookAudioPlayer, staticPlayerBookStateRepository);
    }

    @Provides
    @PerApplication
    AudioPlayerCore provideAudioPlayerCore(){
        return new AudioPlayerCore();
    }

    @Provides
    @PerApplication
    StaticPlayerBookStateRepository provideStaticPlayerBookStateRepository(){
        return new StaticPlayerBookStateRepository();
    }

}
