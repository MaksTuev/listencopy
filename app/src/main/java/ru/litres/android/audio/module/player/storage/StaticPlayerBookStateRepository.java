package ru.litres.android.audio.module.player.storage;


import javax.inject.Inject;

import rx.Observable;

/**
 * хранилище для {@link StaticPlayerBookState}
 * в хранилище не хранится информация о статусе плеера
 */
public class StaticPlayerBookStateRepository {

    @Inject
    public StaticPlayerBookStateRepository() {
    }

    public Observable<StaticPlayerBookState> getStaticPlayerBookState(String bookId) {
        return null;
        //todo
    }
}
