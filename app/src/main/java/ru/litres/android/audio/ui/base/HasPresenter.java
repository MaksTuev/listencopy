package ru.litres.android.audio.ui.base;

public interface HasPresenter {
    BasePresenter getPresenter();
    void initPresenter();
}
